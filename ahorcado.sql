﻿# Host: localhost  (Version: 5.1.67-community)
# Date: 2014-09-16 19:35:09
# Generator: MySQL-Front 5.3  (Build 4.30)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "jugadores"
#

DROP TABLE IF EXISTS `jugadores`;
CREATE TABLE `jugadores` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL DEFAULT '',
  `apellidos` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

#
# Data for table "jugadores"
#

INSERT INTO `jugadores` VALUES (1,'Hugo','Mercado');

#
# Structure for table "palabras"
#

DROP TABLE IF EXISTS `palabras`;
CREATE TABLE `palabras` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(255) NOT NULL DEFAULT '',
  `palabra` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

#
# Data for table "palabras"
#

INSERT INTO `palabras` VALUES (1,'CIUDADES','CARTAGENA'),(2,'CIUDADES','BARRANQUILLA');
