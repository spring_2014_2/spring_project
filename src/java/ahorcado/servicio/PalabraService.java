package ahorcado.servicio;

import ahorcado.modelo.Palabra;
import ahorcado.modelo.jdbc.PalabraJDBC;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author Hugo
 */
public class PalabraService {
    private PalabraJDBC jdbc;
    private Set<String> letras = new HashSet<String>();
    private String palabra;

    public void setJdbc(PalabraJDBC jdbc) {
        this.jdbc = jdbc;
    }

    /**
     * Obtiene una palabra de forma aleatoria
     */
    public String getPalabraAleatoria() {
        String[] palabras = {"CARTAGENA",
            "BARRANQUILLA", "SINCELEJO", "MONTERIA", "BOGOTA", "CALI", "PEREIRA",
            "MEDELLIN", "LETICIA", "PASTO"};
        Random random = new Random();
        palabra = palabras[random.nextInt(10)];
        return palabra;
    }
    
    /**
     * Obtiene la palabra en el formato del juego egun las letras que se hayan encontrado.
     * @param letra
     */
    public String getPalabra(String letra) {
        // revisa el estado de la letra o palabra y la agrega a la lista de letras.
        if(letra != null && !letra.isEmpty() && letra.length() == 1) {
            letras.add(letra);
        } else {
            if(letra.equals(palabra)) {
                return palabra;
            }
        }
        return mostrarPalabra();
    }
    
    /**
     * Verifica si cada una de las letras que están en la palabra se encuentran
     * en la lista de letras.
     */
    private String mostrarPalabra() {
        String palabraParaAdivinar = "";
        String letraEnPalabra = "";
        for (int i = 0; i < palabra.length(); i++) {
            boolean letraExistente = false;
            letraEnPalabra = String.valueOf(palabra.charAt(i));
            for(String letra : letras) {
                if(letraEnPalabra.equals(letra)) {
                    letraExistente = true;
                    break;
                }
            }
            if (letraExistente) {
                palabraParaAdivinar += letraEnPalabra;
            } else {
                palabraParaAdivinar += "_";
            }
            palabraParaAdivinar += " ";
        }
        return palabraParaAdivinar;
    }
    
    public List<Palabra> getPalabras() {
        return jdbc.listPalabras();
    }
}
