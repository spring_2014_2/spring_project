/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ahorcado.controlador;

import ahorcado.servicio.PalabraService;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author Hugo
 */
@ManagedBean
@ViewScoped
public class PalabraBean {

    @ManagedProperty(value="#{palabraService}")
    private PalabraService servicio;
    private String palabra;
    private String palabraEscondida;
    private String letra = "";
    private boolean juegoTerminado = false;

    @PostConstruct
    public void init() {
        if(servicio!=null){
            palabra = servicio.getPalabraAleatoria();
            // se busca la palabra.
            palabraEscondida = servicio.getPalabra(letra);
        }
    }

    public void adivinarPalabra(ActionEvent e) {
        // elimina los espacio que pueda tener la letra al comienzo o al final de esta.
        letra = letra.trim();
        // Genera un objeto string en letras mayusculas.
        letra = letra.toUpperCase();
        // se busca la palabra.
        palabraEscondida = servicio.getPalabra(letra);
        // se asigna un string vació para reiniciar el campo de texto en la vista.
        letra = "";
    }

    //<editor-fold defaultstate="collapsed" desc="GET & SET">
    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getPalabra() {
        return palabra;
    }

    public String getLetra() {
        return letra;
    }

    public void setLetra(String letra) {
        this.letra = letra;
    }

    public String getPalabraEscondida() {
        return palabraEscondida;
    }

    public void setPalabraEscondida(String palabraEscondida) {
        this.palabraEscondida = palabraEscondida;
    }

    public PalabraService getServicio() {
        return servicio;
    }

    public void setServicio(PalabraService servicio) {
        this.servicio = servicio;
    }
    
    public boolean isJuegoTerminado() {
        return juegoTerminado;
    }

    public void setJuegoTerminado(boolean juegoTerminado) {
        this.juegoTerminado = juegoTerminado;
    }    
    //</editor-fold>
}
