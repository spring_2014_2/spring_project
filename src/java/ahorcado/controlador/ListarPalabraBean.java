package ahorcado.controlador;

import ahorcado.servicio.PalabraService;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ListarPalabraBean {

    @ManagedProperty(value="#{palabraService}")
    private PalabraService servicio;

    public PalabraService getServicio() {
        return servicio;
    }

    public void setServicio(PalabraService servicio) {
        this.servicio = servicio;
    }
}
