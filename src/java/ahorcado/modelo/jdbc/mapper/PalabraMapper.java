package ahorcado.modelo.jdbc.mapper;

import ahorcado.modelo.Palabra;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class PalabraMapper implements RowMapper<Palabra> {

    @Override
    public Palabra mapRow(ResultSet rs, int i) throws SQLException {
        Palabra palabra = new Palabra();
        palabra.setId(rs.getInt("id"));
        palabra.setCategoria(rs.getString("categoria"));
        palabra.setPalabra(rs.getString("palabra"));
        return palabra;
    }
}
