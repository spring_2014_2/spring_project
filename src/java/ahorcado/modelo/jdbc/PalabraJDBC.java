package ahorcado.modelo.jdbc;

import ahorcado.modelo.Palabra;
import ahorcado.modelo.jdbc.mapper.PalabraMapper;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

public class PalabraJDBC {
    private JdbcTemplate jdbcTemplate;
    private PalabraMapper mapper;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setMapper(PalabraMapper mapper) {
        this.mapper = mapper;
    }
    
    public Palabra getPalabra(Integer id) {
      String SQL = "SELECT * FROM palabras WHERE id = ?";
      Palabra jugador = jdbcTemplate.queryForObject(SQL, 
                        new Object[]{id}, mapper);
      return jugador;
   }
    
    public void create(String nombres, String apellidos) {
      String SQL = "INSERT INTO palabras (nombres, apellidos) VALUES (?, ?)";
      jdbcTemplate.update( SQL, nombres, apellidos);
   }

   public List<Palabra> listPalabras() {
      String SQL = "SELECT * FROM palabras";
      List <Palabra> jugadores = jdbcTemplate.query(SQL, mapper);
      return jugadores;
   }

   public void delete(Integer id){
      String SQL = "DELETE FROM palabras where id = ?";
      jdbcTemplate.update(SQL, id);
   }

   public void update(Integer id, String nombres, String apellidos){
      String SQL = "UPDATE palabras SET nombres = ?, apellidos=? WHERE id = ?";
      jdbcTemplate.update(SQL, nombres, apellidos);
   }
}
